const { Client } = require("revolt.js");
const fs = require('fs');
const { prefix, token } = require('./config.json');

const client = new Client();

client.on("ready", async () => {
  console.info(`Listo! ${client.user.username} esta encendida`);
});

client.on("messageCreate", async (message) => {

// Declarar esto aqui no es buena idea porque los cosos de abajo probablemente lo rompan pero no lo areglare
  if (message.author.username == client.user.username) return;

  const args = message.content.slice(prefix.length).trim().split(/ +/);
  const commandName = args.shift().toLowerCase();

  try {
    if(!message.content.startsWith(prefix)) return;
    const command = require(`./comandos/${commandName}.js`);
    command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.channel.sendMessage('Hubo un error al ejecutar el comando.');
  }
});

client.loginBot(token);

