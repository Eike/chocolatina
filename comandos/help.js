const fs = require('fs');
const Embed = require("../funciones/embed.js");

module.exports = {
  name: 'help',
  description: 'Muestra una lista de comandos disponibles.',
  category: 'Utilidad',
  execute(message, args) {

    const commandFiles = fs.readdirSync('./comandos').filter(file => file.endsWith('.js'));
    const commands = commandFiles.map(file => require(`./${file}`));

    const categorizedCommands = {};
    const uncategorizedCommands = [];

    commands.forEach(command => {
      const { category, name, description } = command;
      const formattedCommand = `\`${name}\`: ${description}`;
      if (!category) {
        uncategorizedCommands.push(formattedCommand);
      } else {
        const categoryKey = category.toLowerCase();
        if (!categorizedCommands[categoryKey]) {
          categorizedCommands[categoryKey] = [];
        }
        categorizedCommands[categoryKey].push(formattedCommand);
      }
    });

    const requestedCategory = args.join(' ').toLowerCase();
    if (args.length === 0) {
      const categoryList = Object.keys(categorizedCommands).map(category => `• ${category.charAt(0).toUpperCase() + category.slice(1)}`).join('\n');
      
      const embed = new Embed()
        .setTitle(`Lista de comandos de Chocolatina`)
        .setDescription(`### Si quieres ver el listado de comandos de una categoría:\n• ch!help [categoría] \n### 🔍 Categorías\n\n${categoryList}`)
        .setColor(`#F0E2B6`);
      
      //embed.addField(`[Servidor de soporte](http://placeholder)`, `Chocolatina v1.0.0`);
      
      message.channel.sendMessage({ embeds: [embed] });
    } else {
      const categoryCommands = categorizedCommands[requestedCategory];
      if (!categoryCommands) {
        const embed = new Embed()
          .setDescription(`No se encontraron comandos en la categoría **${requestedCategory}**.`)
          .setColor(`#F0E2B6`);
        
        message.channel.sendMessage({ embeds: [embed] });
      } else {
        const categoryMessage = categoryCommands.join('\n');
        
        const embed = new Embed()
          .setTitle(`Comandos en la categoría ${requestedCategory.charAt(0).toUpperCase() + requestedCategory.slice(1)}`)
          .setDescription(categoryMessage)
          .setColor(`#F0E2B6`);
        
        message.channel.sendMessage({ embeds: [embed] });
      }
    }
  }
};

